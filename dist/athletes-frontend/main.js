(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_features_features_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/features/features.component */ "./src/app/components/features/features.component.ts");
/* harmony import */ var _components_pricing_pricing_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/pricing/pricing.component */ "./src/app/components/pricing/pricing.component.ts");
/* harmony import */ var _components_webinars_webinars_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/webinars/webinars.component */ "./src/app/components/webinars/webinars.component.ts");
/* harmony import */ var _components_blog_blog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/blog/blog.component */ "./src/app/components/blog/blog.component.ts");
/* harmony import */ var _components_terms_terms_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/terms/terms.component */ "./src/app/components/terms/terms.component.ts");
/* harmony import */ var _components_teams_teams_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/teams/teams.component */ "./src/app/components/teams/teams.component.ts");
/* harmony import */ var _components_athletes_athletes_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/athletes/athletes.component */ "./src/app/components/athletes/athletes.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    { path: 'features', component: _components_features_features_component__WEBPACK_IMPORTED_MODULE_3__["FeaturesComponent"] },
    { path: 'pricing', component: _components_pricing_pricing_component__WEBPACK_IMPORTED_MODULE_4__["PricingComponent"] },
    { path: 'webinars', component: _components_webinars_webinars_component__WEBPACK_IMPORTED_MODULE_5__["WebinarsComponent"] },
    { path: 'blog', component: _components_blog_blog_component__WEBPACK_IMPORTED_MODULE_6__["BlogComponent"] },
    { path: 'terms', component: _components_terms_terms_component__WEBPACK_IMPORTED_MODULE_7__["TermsComponent"] },
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: 'teams', component: _components_teams_teams_component__WEBPACK_IMPORTED_MODULE_8__["TeamsComponent"] },
    { path: 'athletes', component: _components_athletes_athletes_component__WEBPACK_IMPORTED_MODULE_9__["AthletesComponent"] },
    { path: '', redirectTo: '/', pathMatch: 'full' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <a-navigation></a-navigation>\n\n  <main role=\"main\" class=\"page-container\">\n    <router-outlet></router-outlet>\n  </main>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Athletes Application';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ag-grid-angular */ "./node_modules/ag-grid-angular/main.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ag_grid_angular__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/navigation/navigation.component */ "./src/app/components/navigation/navigation.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_features_features_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/features/features.component */ "./src/app/components/features/features.component.ts");
/* harmony import */ var _components_pricing_pricing_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/pricing/pricing.component */ "./src/app/components/pricing/pricing.component.ts");
/* harmony import */ var _components_webinars_webinars_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/webinars/webinars.component */ "./src/app/components/webinars/webinars.component.ts");
/* harmony import */ var _components_blog_blog_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/blog/blog.component */ "./src/app/components/blog/blog.component.ts");
/* harmony import */ var _components_terms_terms_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/terms/terms.component */ "./src/app/components/terms/terms.component.ts");
/* harmony import */ var _components_teams_teams_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/teams/teams.component */ "./src/app/components/teams/teams.component.ts");
/* harmony import */ var _components_athletes_athletes_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/athletes/athletes.component */ "./src/app/components/athletes/athletes.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_10__["NavigationComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"],
                _components_features_features_component__WEBPACK_IMPORTED_MODULE_12__["FeaturesComponent"],
                _components_pricing_pricing_component__WEBPACK_IMPORTED_MODULE_13__["PricingComponent"],
                _components_webinars_webinars_component__WEBPACK_IMPORTED_MODULE_14__["WebinarsComponent"],
                _components_blog_blog_component__WEBPACK_IMPORTED_MODULE_15__["BlogComponent"],
                _components_terms_terms_component__WEBPACK_IMPORTED_MODULE_16__["TermsComponent"],
                _components_teams_teams_component__WEBPACK_IMPORTED_MODULE_17__["TeamsComponent"],
                _components_athletes_athletes_component__WEBPACK_IMPORTED_MODULE_18__["AthletesComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_6__["BsDropdownModule"].forRoot(),
                ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrModule"].forRoot(),
                ag_grid_angular__WEBPACK_IMPORTED_MODULE_8__["AgGridModule"].withComponents([])
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/athletes/athletes.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/athletes/athletes.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"isLogged && athletes\" style=\"margin: 20px;\">\n  <div class=\"btn-group\" dropdown>\n    <button\n      id=\"button-basic\"\n      dropdownToggle\n      type=\"button\"\n      class=\"btn btn-info dropdown-toggle\"\n      aria-controls=\"dropdown-basic\">\n      {{params.length}}\n      <span class=\"caret\"></span>\n    </button>\n    <ul id=\"dropdown-basic\" *dropdownMenu class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"button-basic\">\n      <li *ngFor=\"let item of itemsPerPage\" role=\"menuitem\">\n        <a class=\"dropdown-item\" (click)=\"setItemsPerPage(item)\">\n          {{item}}\n        </a>\n      </li>\n    </ul>\n  </div>\n\n  <ag-grid-angular \n    style=\"width: 100%; height: 450px;\" \n    class=\"ag-theme-balham\"\n    [rowData]=\"athletes\" \n    [columnDefs]=\"columnDefs\">\n  </ag-grid-angular>\n</div>\n\n<div *ngIf=\"!isLogged\" class=\"access-denided\">\n  <strong>Please, log in to the system first to see athletes!</strong>\n</div>\n  "

/***/ }),

/***/ "./src/app/components/athletes/athletes.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/athletes/athletes.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".access-denided {\n  width: 60%;\n  margin: 0 auto;\n  margin-top: 25px;\n  padding: 20px;\n  background-color: #faf5fa;\n  border: 1px solid red;\n  text-align: center;\n  color: #563d7c; }\n"

/***/ }),

/***/ "./src/app/components/athletes/athletes.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/athletes/athletes.component.ts ***!
  \***********************************************************/
/*! exports provided: AthletesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AthletesComponent", function() { return AthletesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_athletes_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/athletes.service */ "./src/app/services/athletes.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AthletesComponent = /** @class */ (function () {
    function AthletesComponent(auth, athletesService, toastr) {
        this.auth = auth;
        this.athletesService = athletesService;
        this.toastr = toastr;
        this.params = {
            length: 10
        };
        this.itemsPerPage = [10, 25, 50, 100];
        this.columnDefs = [
            { headerName: 'Name', field: 'name' },
            { headerName: 'Domestic market', field: "domestic_market" },
            { headerName: 'Age', field: "age" },
            { headerName: 'Birthday', field: "birthday" },
            { headerName: 'Gender', field: 'gender' },
            { headerName: 'Location Market', field: 'location_market' },
            { headerName: 'Category', field: 'category' }
        ];
    }
    AthletesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth.isUserLogged.subscribe(function (auth_data) {
            _this.isLogged = auth_data.isLoggedIn;
            _this.token = auth_data.accessToken;
        });
        this.isLogged && this.getAllAthletes();
    };
    AthletesComponent.prototype.getAllAthletes = function () {
        var _this = this;
        this.athletesService.getAthletes(this.token, this.params)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])())
            .subscribe(function (result) {
            _this.athletes = result.data;
        }, function (catchedError) {
            _this.showError();
            console.error(catchedError);
        });
    };
    AthletesComponent.prototype.showError = function () {
        this.toastr.error('System is facing with some kind of difficulties. Please, try again later!');
    };
    AthletesComponent.prototype.setItemsPerPage = function (value) {
        if (this.params.length !== value) {
            this.params.length = value;
            this.getAllAthletes();
        }
    };
    AthletesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-athletes',
            template: __webpack_require__(/*! ./athletes.component.html */ "./src/app/components/athletes/athletes.component.html"),
            styles: [__webpack_require__(/*! ./athletes.component.scss */ "./src/app/components/athletes/athletes.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _services_athletes_service__WEBPACK_IMPORTED_MODULE_3__["AthletesService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], AthletesComponent);
    return AthletesComponent;
}());



/***/ }),

/***/ "./src/app/components/blog/blog.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/blog/blog.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  Here will be Blog component!\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/blog/blog.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/blog/blog.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/blog/blog.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/blog/blog.component.ts ***!
  \***************************************************/
/*! exports provided: BlogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogComponent", function() { return BlogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlogComponent = /** @class */ (function () {
    function BlogComponent() {
    }
    BlogComponent.prototype.ngOnInit = function () { };
    BlogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-blog',
            template: __webpack_require__(/*! ./blog.component.html */ "./src/app/components/blog/blog.component.html"),
            styles: [__webpack_require__(/*! ./blog.component.scss */ "./src/app/components/blog/blog.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BlogComponent);
    return BlogComponent;
}());



/***/ }),

/***/ "./src/app/components/features/features.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/features/features.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  Here will be Features component!\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/features/features.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/features/features.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/features/features.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/features/features.component.ts ***!
  \***********************************************************/
/*! exports provided: FeaturesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeaturesComponent", function() { return FeaturesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FeaturesComponent = /** @class */ (function () {
    function FeaturesComponent() {
    }
    FeaturesComponent.prototype.ngOnInit = function () { };
    FeaturesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-features',
            template: __webpack_require__(/*! ./features.component.html */ "./src/app/components/features/features.component.html"),
            styles: [__webpack_require__(/*! ./features.component.scss */ "./src/app/components/features/features.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FeaturesComponent);
    return FeaturesComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!isLogged\" class=\"login-container\">\r\n  <h1>Login</h1>\r\n  <form [formGroup]=\"loginForm\">\r\n    <div class=\"form-group\">\r\n      <label for=\"userName\">Username</label>\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"username\"\r\n        class=\"form-control\"\r\n        id=\"userName\"\r\n        aria-describedby=\"usernameHelp\"\r\n        placeholder=\"Enter username\">\r\n      <small\r\n        *ngIf=\"loginForm.controls['username'].touched && loginForm.controls['username'].invalid\"\r\n        id=\"usernameHelp\"\r\n        class=\"error-msg form-text\">\r\n        {{getErrorMessage(loginForm.controls['username'])}}\r\n      </small>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <label for=\"userPassword\">Password</label>\r\n      <input\r\n        type=\"password\"\r\n        formControlName=\"password\"\r\n        class=\"form-control\"\r\n        id=\"userPassword\"\r\n        aria-describedby=\"passwordHelp\"\r\n        placeholder=\"Password\">\r\n      <small\r\n        *ngIf=\"loginForm.controls['password'].touched && loginForm.controls['password'].invalid\"\r\n        id=\"passwordHelp\"\r\n        class=\"error-msg form-text\">\r\n        {{getErrorMessage(loginForm.controls['password'])}}\r\n      </small>\r\n    </div>\r\n    <button\r\n      type=\"submit\"\r\n      [ngClass]=\"{'disabled': loginForm.invalid}\"\r\n      class=\"btn\"\r\n      [disabled]=\"loginForm.invalid\"\r\n      (click)=\"submit()\">\r\n      Login\r\n    </button>\r\n  </form>\r\n</div>\r\n\r\n<div *ngIf=\"isLogged\" class=\"login-notification jumbotron\">\r\n  <h1 class=\"display-4\">Dear, user!</h1>\r\n  <p class=\"lead\">You are already logged in our system.</p>\r\n  <hr class=\"my-4\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/login/login.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-container {\n  width: 40%;\n  margin: 0 auto;\n  padding: 25px;\n  background-color: #faf5fa; }\n  .login-container button {\n    background-color: #563d7c;\n    color: white; }\n  .login-container .error-msg {\n    color: red; }\n  .login-container .disabled {\n    border: 1px solid #563d7c;\n    background-color: #cccccc;\n    color: #666666;\n    cursor: no-drop; }\n  .login-notification {\n  padding: 10px 20px;\n  text-align: center; }\n"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(auth, router) {
        this.auth = auth;
        this.router = router;
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
        });
        this.errorHandlers = {
            required: function () { return 'This field is required.'; }
        };
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth.isUserLogged.subscribe(function (auth_data) {
            _this.isLogged = auth_data.isLoggedIn;
        });
    };
    LoginComponent.prototype.getErrorMessage = function (control) {
        var errorName = Object.keys(control.errors)[0];
        return this.errorHandlers[errorName](control.errors[errorName]);
    };
    LoginComponent.prototype.submit = function () {
        var _this = this;
        this.auth.login(this.loginForm.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
            .subscribe(function (result) {
            _this.isLogged = result;
            _this.router.navigate(['teams']);
        }, function (catchedError) {
            // todo: we will be doing something if get any issues
            console.log(catchedError);
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/components/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/navigation/navigation.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light\">\r\n  <div class=\"container collapse navbar-collapse\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n      <div *ngIf=\"isLogged\" class=\"btn-group\" dropdown>\r\n        <button\r\n          id=\"button-basic\"\r\n          dropdownToggle\r\n          type=\"button\"\r\n          class=\"btn btn-primary dropdown-toggle\"\r\n          aria-controls=\"dropdown-basic\">\r\n          CRM\r\n          <span class=\"caret\"></span>\r\n        </button>\r\n        <ul id=\"dropdown-basic\" *dropdownMenu class=\"dropdown-menu\"\r\n            role=\"menu\" aria-labelledby=\"button-basic\">\r\n          <li role=\"menuitem\"><a class=\"dropdown-item\" routerLink=\"/teams\">Teams</a></li>\r\n          <li role=\"menuitem\"><a class=\"dropdown-item\" routerLink=\"/athletes\">Athletes</a></li>\r\n        </ul>\r\n      </div>\r\n      <li class=\"nav-item logo\">\r\n        <a class=\"nav-link\" routerLink=\"/\"></a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/features\">Features</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/pricing\">Pricing</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/webinars\">Webinars</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/blog\">Blog</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/terms\">Terms Of Service</a>\r\n      </li>\r\n    </ul>\r\n\r\n    <button *ngIf=\"isLogged\" (click)=\"logOut()\" class=\"btn my-2 my-sm-0\">\r\n      Logout\r\n    </button>\r\n    <button *ngIf=\"!isLogged\" (click)=\"logIn()\" class=\"btn my-2 my-sm-0\">\r\n      Login\r\n    </button>\r\n    <span *ngIf=\"isLogged && userName\">Hello, {{userName}}</span>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/components/navigation/navigation.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav {\n  height: 50px;\n  font-weight: bold; }\n"

/***/ }),

/***/ "./src/app/components/navigation/navigation.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/navigation/navigation.component.ts ***!
  \***************************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    NavigationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth.isUserLogged.subscribe(function (auth_data) {
            _this.isLogged = auth_data.isLoggedIn;
            _this.userName = auth_data.username;
        });
    };
    NavigationComponent.prototype.logIn = function () {
        this.router.navigate(['login']);
    };
    NavigationComponent.prototype.logOut = function () {
        this.auth.logout();
    };
    NavigationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-navigation',
            template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/components/navigation/navigation.component.html"),
            styles: [__webpack_require__(/*! ./navigation.component.scss */ "./src/app/components/navigation/navigation.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "./src/app/components/pricing/pricing.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/pricing/pricing.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  Here will be Pricing component!\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/pricing/pricing.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/pricing/pricing.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/pricing/pricing.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/pricing/pricing.component.ts ***!
  \*********************************************************/
/*! exports provided: PricingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricingComponent", function() { return PricingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PricingComponent = /** @class */ (function () {
    function PricingComponent() {
    }
    PricingComponent.prototype.ngOnInit = function () { };
    PricingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-pricing',
            template: __webpack_require__(/*! ./pricing.component.html */ "./src/app/components/pricing/pricing.component.html"),
            styles: [__webpack_require__(/*! ./pricing.component.scss */ "./src/app/components/pricing/pricing.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PricingComponent);
    return PricingComponent;
}());



/***/ }),

/***/ "./src/app/components/teams/teams.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/teams/teams.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"isLogged && teams\" style=\"margin: 20px;\">\n  <div class=\"btn-group\" dropdown>\n    <button\n      id=\"button-basic\"\n      dropdownToggle\n      type=\"button\"\n      class=\"btn btn-info dropdown-toggle\"\n      aria-controls=\"dropdown-basic\">\n      {{params.length}}\n      <span class=\"caret\"></span>\n    </button>\n    <ul id=\"dropdown-basic\" *dropdownMenu class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"button-basic\">\n      <li *ngFor=\"let item of itemsPerPage\" role=\"menuitem\">\n        <a class=\"dropdown-item\" (click)=\"setItemsPerPage(item)\">\n          {{item}}\n        </a>\n      </li>\n    </ul>\n  </div>\n\n  <ag-grid-angular \n    style=\"width: 100%; height: 450px;\" \n    class=\"ag-theme-balham\"\n    [rowData]=\"teams\" \n    [columnDefs]=\"columnDefs\">\n  </ag-grid-angular>\n</div>\n\n<div *ngIf=\"!isLogged\" class=\"access-denided\">\n  <strong>Please, log in to the system first to see teams!</strong>\n</div>\n"

/***/ }),

/***/ "./src/app/components/teams/teams.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/teams/teams.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".access-denided {\n  width: 60%;\n  margin: 0 auto;\n  margin-top: 25px;\n  padding: 20px;\n  background-color: #faf5fa;\n  border: 1px solid red;\n  text-align: center;\n  color: #563d7c; }\n"

/***/ }),

/***/ "./src/app/components/teams/teams.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/teams/teams.component.ts ***!
  \*****************************************************/
/*! exports provided: TeamsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamsComponent", function() { return TeamsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_teams_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/teams.service */ "./src/app/services/teams.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TeamsComponent = /** @class */ (function () {
    function TeamsComponent(auth, teamsService, toastr) {
        this.auth = auth;
        this.teamsService = teamsService;
        this.toastr = toastr;
        this.params = {
            length: 10
        };
        this.itemsPerPage = [10, 25, 50, 100];
        this.columnDefs = [
            { headerName: 'Name', field: 'name' },
            { headerName: 'Location Market', field: 'location_market' },
            { headerName: 'Gender', field: 'gender' },
            { headerName: 'Category', field: 'category' },
            { headerName: 'League', field: "additional_info.League" },
            { headerName: 'Ground', field: 'additional_info.Ground' },
            { headerName: 'Manager', field: 'additional_info.Manager' }
        ];
    }
    TeamsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth.isUserLogged.subscribe(function (auth_data) {
            _this.isLogged = auth_data.isLoggedIn;
            _this.token = auth_data.accessToken;
        });
        this.isLogged && this.getAllTeams();
    };
    TeamsComponent.prototype.getAllTeams = function () {
        var _this = this;
        this.teamsService.getTeams(this.token, this.params)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["first"])())
            .subscribe(function (result) {
            _this.teams = result.data;
        }, function (catchedError) {
            _this.showError();
            console.error(catchedError);
        });
    };
    TeamsComponent.prototype.showError = function () {
        this.toastr.error('System is facing with some kind of difficulties. Please, try again later!');
    };
    TeamsComponent.prototype.setItemsPerPage = function (value) {
        if (this.params.length !== value) {
            this.params.length = value;
            this.getAllTeams();
        }
    };
    TeamsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-teams',
            template: __webpack_require__(/*! ./teams.component.html */ "./src/app/components/teams/teams.component.html"),
            styles: [__webpack_require__(/*! ./teams.component.scss */ "./src/app/components/teams/teams.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _services_teams_service__WEBPACK_IMPORTED_MODULE_3__["TeamsService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], TeamsComponent);
    return TeamsComponent;
}());



/***/ }),

/***/ "./src/app/components/terms/terms.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/terms/terms.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <div class=\"jumbotron\">\r\n        <h1 class=\"\">TERMS OF SERVICE</h1>\r\n\r\n        <h3 class=\"mt-5\">OVERVIEW</h3>\r\n        This website is operated by Games. Throughout the site, the terms “we”, “us” and “our” refer to Games. Games offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.\r\n        By visiting our site and/ or purchasing something from us, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply  to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content.\r\n        Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service.\r\n        Any new features or tools which are added to the current store shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.\r\n\r\n        <h3 class=\"mt-5\">SECTION 1 - ONLINE STORE TERMS</h3>\r\n        By agreeing to these Terms of Service, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.\r\n        You may not use our products for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).\r\n        You must not transmit any worms or viruses or any code of a destructive nature.\r\n        A breach or violation of any of the Terms will result in an immediate termination of your Services.\r\n\r\n\r\n        <h3 class=\"mt-5\">SECTION 2 - GENERAL CONDITIONS</h3>\r\n        We reserve the right to refuse service to anyone for any reason at any time.\r\n        You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks.\r\n        You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website through which the service is provided, without express written permission by us.\r\n        The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.\r\n\r\n        <h3 class=\"mt-5\">SECTION 3 - ACCURACY, COMPLETENESS AND TIMELINESS OF INFORMATION</h3>\r\n        We are not responsible if information made available on this site is not accurate, complete or current. The material on this site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or more timely sources of information. Any reliance on the material on this site is at your own risk.\r\n        This site may contain certain historical information. Historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site.\r\n\r\n        <h3 class=\"mt-5\">SECTION 4 - MODIFICATIONS TO THE SERVICE AND PRICES</h3>3>\r\n        Prices for our products are subject to change without notice.\r\n        We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time.\r\n        We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.\r\n\r\n        <h3 class=\"mt-5\">SECTION 5 - PRODUCTS OR SERVICES (if applicable)</h3>\r\n        Certain products or services may be available exclusively online through the website. These products or services may have limited quantities and are subject to return or exchange only according to our Return Policy.\r\n        We have made every effort to display as accurately as possible the colors and images of our products that appear at the store. We cannot guarantee that your computer monitor's display of any color will be accurate.\r\n        We reserve the right, but are not obligated, to limit the sales of our products or Services to any person, geographic region or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right to limit the quantities of any products or services that we offer. All descriptions of products or product pricing are subject to change at anytime without notice, at the sole discretion of us. We reserve the right to discontinue any product at any time. Any offer for any product or service made on this site is void where prohibited.\r\n        We do not warrant that the quality of any products, services, information, or other material purchased or obtained by you will meet your expectations, or that any errors in the Service will be corrected.\r\n\r\n        <h3 class=\"mt-5\">SECTION 6 - ACCURACY OF BILLING AND ACCOUNT INFORMATION</h3>\r\n        We reserve the right to refuse any order you place with us. We may, in our sole discretion, limit or cancel quantities purchased per person, per household or per order. These restrictions may include orders placed by or under the same customer account, the same credit card, and/or orders that use the same billing and/or shipping address. In the event that we make a change to or cancel an order, we may attempt to notify you by contacting the e-mail and/or billing address/phone number provided at the time the order was made. We reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distributors.\r\n        You agree to provide current, complete and accurate purchase and account information for all purchases made at our store. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.\r\n        For more detail, please review our Returns Policy.\r\n\r\n        <h3 class=\"mt-5\">SECTION 7 - OPTIONAL TOOLS</h3>\r\n        We may provide you with access to third-party tools over which we neither monitor nor have any control nor input.\r\n        You acknowledge and agree that we provide access to such tools ”as is” and “as available” without any warranties, representations or conditions of any kind and without any endorsement. We shall have no liability whatsoever arising from or relating to your use of optional third-party tools.\r\n        Any use by you of optional tools offered through the site is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s).\r\n        We may also, in the future, offer new services and/or features through the website (including, the release of new tools and resources). Such new features and/or services shall also be subject to these Terms of Service.\r\n\r\n        <h3 class=\"mt-5\">SECTION 8 - THIRD-PARTY LINKS</h3>\r\n        Certain content, products and services available via our Service may include materials from third-parties.\r\n        Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.\r\n        We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party's policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.\r\n\r\n        <h3 class=\"mt-5\">SECTION 9 - USER COMMENTS, FEEDBACK AND OTHER SUBMISSIONS</h3>\r\n        If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, 'comments'), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments.\r\n        We may, but have no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms of Service.\r\n        You agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.\r\n\r\n        <h3 class=\"mt-5\">SECTION 10 - PERSONAL INFORMATION</h3>\r\n        Your submission of personal information through the store is governed by our Privacy Policy. To view our Privacy Policy.\r\n\r\n        <h3 class=\"mt-5\">SECTION 11 - ERRORS, INACCURACIES AND OMISSIONS</h3>\r\n        Occasionally there may be information on our site or in the Service that contains typographical errors, inaccuracies or omissions that may relate to product descriptions, pricing, promotions, offers, product shipping charges, transit times and availability. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information in the Service or on any related website is inaccurate at any time without prior notice (including after you have submitted your order).\r\n        We undertake no obligation to update, amend or clarify information in the Service or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Service or on any related website, should be taken to indicate that all information in the Service or on any related website has been modified or updated.\r\n\r\n        <h3 class=\"mt-5\">SECTION 12 - PROHIBITED USES</h3>\r\n        In addition to other prohibitions as set forth in the Terms of Service, you are prohibited from using the site or its content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.\r\n\r\n        <h3 class=\"mt-5\">SECTION 13 - DISCLAIMER OF WARRANTIES; LIMITATION OF LIABILITY</h3>\r\n        We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free.\r\n        We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable.\r\n        You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you.\r\n        You expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all products and services delivered to you through the service are (except as expressly stated by us) provided 'as is' and 'as available' for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement.\r\n        In no case shall Games, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using the service, or for any other claim related in any way to your use of the service or any product, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or product) posted, transmitted, or otherwise made available via the service, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law.\r\n\r\n        <h3 class=\"mt-5\">SECTION 14 - INDEMNIFICATION</h3>\r\n        You agree to indemnify, defend and hold harmless Games and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third-party due to or arising out of your breach of these Terms of Service or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.\r\n\r\n        <h3 class=\"mt-5\">SECTION 15 - SEVERABILITY</h3>\r\n        In the event that any provision of these Terms of Service is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms of Service, such determination shall not affect the validity and enforceability of any other remaining provisions.\r\n\r\n        <h3 class=\"mt-5\">SECTION 16 - TERMINATION</h3>\r\n        The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes.\r\n        These Terms of Service are effective unless and until terminated by either you or us. You may terminate these Terms of Service at any time by notifying us that you no longer wish to use our Services, or when you cease using our site.\r\n        If in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms of Service, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).\r\n\r\n        <h3 class=\"mt-5\">SECTION 17 - ENTIRE AGREEMENT</h3>\r\n        The failure of us to exercise or enforce any right or provision of these Terms of Service shall not constitute a waiver of such right or provision.\r\n        These Terms of Service and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Service).\r\n        Any ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting party.\r\n\r\n        <h3 class=\"mt-5\">SECTION 18 - GOVERNING LAW</h3>\r\n        These Terms of Service and any separate agreements whereby we provide you Services shall be governed by and construed in accordance with the laws of .\r\n\r\n        <h3 class=\"mt-5\">SECTION 19 - CHANGES TO TERMS OF SERVICE</h3>\r\n        You can review the most current version of the Terms of Service at any time at this page.\r\n        We reserve the right, at our sole discretion, to update, change or replace any part of these Terms of Service by posting updates and changes to our website. It is your responsibility to check our website periodically for changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms of Service constitutes acceptance of those changes.\r\n\r\n        <h3 class=\"mt-5\">SECTION 20 - CONTACT INFORMATION</h3>\r\n        Questions about the Terms of Service should be sent to us at mriynuk@gmail.com.\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/terms/terms.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/terms/terms.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/terms/terms.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/terms/terms.component.ts ***!
  \*****************************************************/
/*! exports provided: TermsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsComponent", function() { return TermsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TermsComponent = /** @class */ (function () {
    function TermsComponent() {
    }
    TermsComponent.prototype.ngOnInit = function () { };
    TermsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-terms',
            template: __webpack_require__(/*! ./terms.component.html */ "./src/app/components/terms/terms.component.html"),
            styles: [__webpack_require__(/*! ./terms.component.scss */ "./src/app/components/terms/terms.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TermsComponent);
    return TermsComponent;
}());



/***/ }),

/***/ "./src/app/components/webinars/webinars.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/webinars/webinars.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  Here will be Webinars component!\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/webinars/webinars.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/webinars/webinars.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/webinars/webinars.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/webinars/webinars.component.ts ***!
  \***********************************************************/
/*! exports provided: WebinarsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebinarsComponent", function() { return WebinarsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WebinarsComponent = /** @class */ (function () {
    function WebinarsComponent() {
    }
    WebinarsComponent.prototype.ngOnInit = function () { };
    WebinarsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'a-webinars',
            template: __webpack_require__(/*! ./webinars.component.html */ "./src/app/components/webinars/webinars.component.html"),
            styles: [__webpack_require__(/*! ./webinars.component.scss */ "./src/app/components/webinars/webinars.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], WebinarsComponent);
    return WebinarsComponent;
}());



/***/ }),

/***/ "./src/app/constants/constants.ts":
/*!****************************************!*\
  !*** ./src/app/constants/constants.ts ***!
  \****************************************/
/*! exports provided: constants */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "constants", function() { return constants; });
var constants = {
    host: 'http://athletes.mkeda.me',
    routes: {
        login: '/api/token',
        teams: '/api/teams',
        athletes: '/api/athletes'
    }
};


/***/ }),

/***/ "./src/app/services/athletes.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/athletes.service.ts ***!
  \**********************************************/
/*! exports provided: AthletesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AthletesService", function() { return AthletesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AthletesService = /** @class */ (function () {
    function AthletesService(http) {
        this.http = http;
    }
    AthletesService.prototype.getAthletes = function (token, params) {
        var url;
        if (params && params.length) {
            url = "" + _constants_constants__WEBPACK_IMPORTED_MODULE_3__["constants"].host + _constants_constants__WEBPACK_IMPORTED_MODULE_3__["constants"].routes.athletes + "?length=" + params.length;
        }
        else {
            url = "" + _constants_constants__WEBPACK_IMPORTED_MODULE_3__["constants"].host + _constants_constants__WEBPACK_IMPORTED_MODULE_3__["constants"].routes.athletes;
        }
        return this.http.get(url, {
            headers: this.getHeaders(token)
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    AthletesService.prototype.getHeaders = function (jwt_token) {
        return {
            Authorization: "JWT " + jwt_token
        };
    };
    AthletesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AthletesService);
    return AthletesService;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.isUserLogged = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({
            isLoggedIn: this.isLoggedIn,
            accessToken: this.accessToken || null,
            username: this.userName || null
        });
    }
    Object.defineProperty(AuthService.prototype, "isLoggedIn", {
        get: function () {
            return (localStorage.getItem('accessToken') !== null);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "userName", {
        get: function () {
            return localStorage.getItem('username');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "accessToken", {
        get: function () {
            return localStorage.getItem('accessToken');
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.eventChange = function (access_token, user_name) {
        if (access_token && user_name) {
            this.isUserLogged.next({ isLoggedIn: true, accessToken: access_token, username: user_name });
        }
        else {
            this.isUserLogged.next({ isLoggedIn: false, accessToken: null, username: null });
        }
    };
    AuthService.prototype.login = function (authData) {
        var _this = this;
        return this.http.post("" + _constants_constants__WEBPACK_IMPORTED_MODULE_4__["constants"].host + _constants_constants__WEBPACK_IMPORTED_MODULE_4__["constants"].routes.login, authData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            localStorage.setItem('accessToken', result.access);
            localStorage.setItem('username', result.username);
            _this.eventChange(result.access, result.username);
            return true;
        }));
    };
    AuthService.prototype.logout = function () {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('username');
        this.eventChange(JSON.parse(localStorage.getItem('accessToken')), null);
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/teams.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/teams.service.ts ***!
  \*******************************************/
/*! exports provided: TeamsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamsService", function() { return TeamsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TeamsService = /** @class */ (function () {
    function TeamsService(http) {
        this.http = http;
    }
    TeamsService.prototype.getTeams = function (token, params) {
        var url;
        if (params && params.length) {
            url = "" + _constants_constants__WEBPACK_IMPORTED_MODULE_3__["constants"].host + _constants_constants__WEBPACK_IMPORTED_MODULE_3__["constants"].routes.teams + "?length=" + params.length;
        }
        else {
            url = "" + _constants_constants__WEBPACK_IMPORTED_MODULE_3__["constants"].host + _constants_constants__WEBPACK_IMPORTED_MODULE_3__["constants"].routes.teams;
        }
        return this.http.get(url, {
            headers: this.getHeaders(token)
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    TeamsService.prototype.getHeaders = function (jwt_token) {
        return {
            Authorization: "JWT " + jwt_token
        };
    };
    TeamsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], TeamsService);
    return TeamsService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Athlets\athletes-frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map