import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {AuthService} from '../../services/auth.service';
import {TeamsService} from '../../services/teams.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'a-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})

export class TeamsComponent implements OnInit {
  isLogged: boolean;
  token: string;
  teams: any;
  params = {
    length: 10
  };
  itemsPerPage = [10, 25, 50, 100];

  columnDefs = [
    {headerName: 'Name', field: 'name' },
    {headerName: 'Location Market', field: 'location_market'},
    {headerName: 'Gender', field: 'gender'},
    {headerName: 'Category', field: 'category'},
    {headerName: 'League', field: "additional_info.League"},
    {headerName: 'Ground', field: 'additional_info.Ground'},
    {headerName: 'Manager', field: 'additional_info.Manager'}
  ];

  constructor(
    private auth: AuthService,
    private teamsService: TeamsService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.auth.isUserLogged.subscribe(auth_data => {
      this.isLogged = auth_data.isLoggedIn;
      this.token = auth_data.accessToken;
    });
    this.isLogged && this.getAllTeams();
  }

  getAllTeams() {
    this.teamsService.getTeams(this.token, this.params)
      .pipe(first())
      .subscribe(result => {
        this.teams = result.data;
      }, catchedError => {
        this.showError();
        console.error(catchedError);
      });
  }

  showError() {
    this.toastr.error('System is facing with some kind of difficulties. Please, try again later!');
  }

  setItemsPerPage(value) {
    if (this.params.length !== value) {
      this.params.length = value;
      this.getAllTeams();
    }
  }
}
