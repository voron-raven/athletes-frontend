import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'a-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });
  isLogged: boolean;

  errorHandlers = {
    required: () => 'This field is required.'
  };

  constructor(
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.auth.isUserLogged.subscribe(auth_data => {
      this.isLogged = auth_data.isLoggedIn;
    });
  }

  getErrorMessage(control: any) {
    const errorName = Object.keys(control.errors)[0];
    return this.errorHandlers[errorName](control.errors[errorName]);
  }

  submit() {
    this.auth.login(this.loginForm.value)
      .pipe(first())
      .subscribe(result => {
        this.isLogged = result;
        this.router.navigate(['teams']);
      },
      catchedError => {
        // todo: we will be doing something if get any issues
        console.log(catchedError);
      });
  }
}
