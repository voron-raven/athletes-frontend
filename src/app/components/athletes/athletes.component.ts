import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {AuthService} from '../../services/auth.service';
import {AthletesService} from '../../services/athletes.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'a-athletes',
  templateUrl: './athletes.component.html',
  styleUrls: ['./athletes.component.scss']
})

export class AthletesComponent implements OnInit {
  isLogged: boolean;
  token: string;
  athletes: any;
  params = {
    length: 10
  };
  itemsPerPage = [10, 25, 50, 100];

  columnDefs = [
    {headerName: 'Name', field: 'name' },
    {headerName: 'Domestic market', field: "domestic_market"},
    {headerName: 'Age', field: "age"},
    {headerName: 'Birthday', field: "birthday"},
    {headerName: 'Gender', field: 'gender'},
    {headerName: 'Location Market', field: 'location_market'},
    {headerName: 'Category', field: 'category'}
  ];

  constructor(
    private auth: AuthService,
    private athletesService: AthletesService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.auth.isUserLogged.subscribe(auth_data => {
      this.isLogged = auth_data.isLoggedIn;
      this.token = auth_data.accessToken;
    });
    this.isLogged && this.getAllAthletes();
  }

  getAllAthletes() {
    this.athletesService.getAthletes(this.token, this.params)
      .pipe(first())
      .subscribe(result => {
        this.athletes = result.data;
      }, catchedError => {
        this.showError();
        console.error(catchedError);
      });
  }

  showError() {
    this.toastr.error('System is facing with some kind of difficulties. Please, try again later!');
  }

  setItemsPerPage(value) {
    if (this.params.length !== value) {
      this.params.length = value;
      this.getAllAthletes();
    }
  }
}
