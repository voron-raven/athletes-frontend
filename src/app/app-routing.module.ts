import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LoginComponent} from './components/login/login.component';
import {FeaturesComponent} from './components/features/features.component';
import {PricingComponent} from './components/pricing/pricing.component';
import {WebinarsComponent} from './components/webinars/webinars.component';
import {BlogComponent} from './components/blog/blog.component';
import {TermsComponent} from './components/terms/terms.component';
import {TeamsComponent} from './components/teams/teams.component';
import {AthletesComponent} from './components/athletes/athletes.component';

const routes: Routes = [
  {path: 'features', component: FeaturesComponent},
  {path: 'pricing', component: PricingComponent},
  {path: 'webinars', component: WebinarsComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'terms', component: TermsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'teams', component: TeamsComponent},
  {path: 'athletes', component: AthletesComponent},
  {path: '', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule {}
