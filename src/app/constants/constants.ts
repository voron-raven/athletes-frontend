export const constants = {
    host: 'http://athletes.mkeda.me',
    routes: {
      login: '/api/token',
      teams: '/api/teams',
      athletes: '/api/athletes'
    }
};
