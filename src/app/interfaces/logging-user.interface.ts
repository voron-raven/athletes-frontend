export interface LoggingUser {
    refresh: string;
    access: string;
    username: string;
}
