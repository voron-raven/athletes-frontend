export interface Athletes {
    draw: number;
    recordsTotal: number;
    recordsFiltered: number;
    data: any;
}
