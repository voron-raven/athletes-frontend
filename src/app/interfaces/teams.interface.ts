export interface Teams {
    draw: number;
    recordsTotal: number;
    recordsFiltered: number;
    data: any;
}
