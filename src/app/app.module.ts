import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ToastrModule} from 'ngx-toastr';
import {AgGridModule} from 'ag-grid-angular';

import {AppComponent} from './app.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {LoginComponent} from './components/login/login.component';
import {FeaturesComponent} from './components/features/features.component';
import {PricingComponent} from './components/pricing/pricing.component';
import {WebinarsComponent} from './components/webinars/webinars.component';
import {BlogComponent} from './components/blog/blog.component';
import {TermsComponent} from './components/terms/terms.component';
import {TeamsComponent} from './components/teams/teams.component';
import {AthletesComponent} from './components/athletes/athletes.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    FeaturesComponent,
    PricingComponent,
    WebinarsComponent,
    BlogComponent,
    TermsComponent,
    TeamsComponent,
    AthletesComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot(),
    AgGridModule.withComponents([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {}
