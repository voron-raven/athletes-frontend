import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {constants} from '../constants/constants';
import {Athletes} from '../interfaces/athletes.interface';

@Injectable({
  providedIn: 'root'
})

export class AthletesService {
  constructor(private http: HttpClient) {}

  getAthletes(token: string, params: any): Observable<Athletes> {
    let url;
    if (params && params.length) {
      url = `${constants.host}${constants.routes.athletes}?length=${params.length}`;
    } else {
      url = `${constants.host}${constants.routes.athletes}`;
    }

    return this.http.get<Athletes>(url, {
      headers: this.getHeaders(token)
    })
      .pipe(
        map(result => {
          return result;
        })
      )
  }

  getHeaders(jwt_token) {
    return {
      Authorization: `JWT ${jwt_token}`
    }
  }
}
