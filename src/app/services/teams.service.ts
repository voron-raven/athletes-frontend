import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {constants} from '../constants/constants';
import {Teams} from '../interfaces/teams.interface';

@Injectable({
  providedIn: 'root'
})

export class TeamsService {

  constructor(private http: HttpClient) {}

  getTeams(token: string, params: any): Observable<Teams> {
    let url;
    if (params && params.length) {
      url = `${constants.host}${constants.routes.teams}?length=${params.length}`;
    } else {
      url = `${constants.host}${constants.routes.teams}`;
    }

    return this.http.get<Teams>(url, {
      headers: this.getHeaders(token)
    })
      .pipe(
        map(result => {
          return result;
        })
      )
  }

  getHeaders(jwt_token) {
    return {
      Authorization: `JWT ${jwt_token}`
    }
  }
}
