import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, BehaviorSubject} from 'rxjs';
import {map} from 'rxjs/operators';
import {constants} from '../constants/constants';
import {LoggingUser} from '../interfaces/logging-user.interface';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  isUserLogged = new BehaviorSubject({
    isLoggedIn: this.isLoggedIn,
    accessToken: this.accessToken || null,
    username: this.userName || null
  });

  constructor(private http: HttpClient) {}

  public get isLoggedIn() {
    return (localStorage.getItem('accessToken') !== null);
  }

  public get userName() {
    return localStorage.getItem('username');
  }

  public get accessToken() {
    return localStorage.getItem('accessToken');
  }

  eventChange(access_token, user_name) {
    if (access_token && user_name) {
      this.isUserLogged.next({isLoggedIn: true, accessToken: access_token, username: user_name});
    } else {
      this.isUserLogged.next({isLoggedIn: false, accessToken: null, username: null});
    }
  }

  login(authData: any): Observable<boolean> {
    return this.http.post<LoggingUser>(`${constants.host}${constants.routes.login}`, authData)
      .pipe(
        map(result => {
          localStorage.setItem('accessToken', result.access);
          localStorage.setItem('username', result.username);
          this.eventChange(result.access, result.username);
          return true;
        })
      );
  }

  logout() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('username');
    this.eventChange(JSON.parse(localStorage.getItem('accessToken')), null);
  }
}
